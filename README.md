# mock-app-store


The idea is built a mock app store with web technology.
Task:
 
1. Horizontal Scroll for 10 top-grossing apps
2. Veritical Scroll with lazy loading 10 top-free apps at one time.
3. An instant search.

```
git clone https://gitlab.com/koloyyee/ios-app 
 // or
git clone git@gitlab.com:koloyyee/ios-app.git 

or

git clone https://github.com/koloyyee/mock-app-store.git 
 // or
git clone git@github.com:koloyyee/mock-app-store.git


cd react 
yarn install
yarn start

cd server
yarn install
yarn node-dev main.js
```
